# springCloud-eureka

#### 介绍
Eureka（服务注册和发现）的使用案例

#### 软件架构



![输入图片说明](https://foruda.gitee.com/images/1697080725015933190/7617aa84_9422135.png "屏幕截图")


[Eureka（服务注册和发现）——Eureka的简介和原理 & Eureka的使用和分析 & 心跳续约策略，服务的下线和剔除，自我保护 & Eureka集群的搭建](https://blog.csdn.net/Pireley/article/details/133784749)



![输入图片说明](https://foruda.gitee.com/images/1697080786969029247/a5f418df_9422135.png "屏幕截图")