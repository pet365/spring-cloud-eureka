package com.tianju.provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class ProviderController {
    @GetMapping("get/{id}")
    public String sendMsg(@PathVariable("id") String id){
        return "我是服务提供者：生产者,获得参数id="+id;
    }
}
