package com.tianju.consumer.controller;

import com.netflix.appinfo.InstanceInfo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/consumer")
@Slf4j
public class ConsumerController {

    @Resource
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/getId/{id}")
    public String getMsg(@PathVariable("id") String id) {
        List<ServiceInstance> instances = discoveryClient.getInstances("springCloud-provider");
        ServiceInstance instance = instances.get(0);
        String host = instance.getHost();
        int port = instance.getPort();
        log.debug("消费者拼出路径+端口："+ host+":"+port);

        // 获取ip和端口信息，拼接成服务地址
        String baseUrl = "http://" + instance.getHost() + ":" +
                instance.getPort() + "/provider/get/" + id;
        String consumer = restTemplate.getForObject(baseUrl, String.class);

        log.debug("采用restTemplate调用生产者："+baseUrl);
        return "消费者调用生产者获得消息："+consumer;
    }
}
